var bCrypt = require('bcrypt-nodejs');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var users = mongoose.model('user');

module.exports = function (passport) {
    passport.use('login', new LocalStrategy({
        passReqToCallback : true,// allows us to pass back the entire request to the callback
        usernameField: 'username',
		passwordField: 'password'
    },
  function (req, username, password, done) {
        // check in mongo if a user with username exists or not
		users.findOne({ 'username' : username }, function (err, user) {
			//console.log(user);
            // In case of any error, return using the done method
            if (err)
                return done(err);
            // Username does not exist, log error & redirect back
            if (!user) {
                console.log('User Not Found with username ' + username);
                ret = req.flash('warning', 'User Not Found with username ' + username);
                return done(null, false, ret);
            }
            // User exists but wrong password, log the error 
            if (!isValidPassword(user, password)) {
                console.log('Invalid Password');
                return done(null, false, req.flash('warning', 'Invalid Password'));
            }
            // User and password both match, return user from 
            // done method which will be treated like success
            req.flash('warning', 'Login successful');
            //console.log('user:',user);
            return done(null, user);
        });
    }));

    var isValidPassword = function (user, password) {
        return bCrypt.compareSync(password, user.password);
    }
}


