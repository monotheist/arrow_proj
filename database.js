﻿var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var arrow = new Schema(
	{
		title : String,
		date : String,
		img_link: String,
		read_more: String,
		mode: String
	}
);
var contactRequests = new Schema(
	{
		queried:String,
		name:String,
		email:String,
		phone:String
	}
);
var user = new Schema({
	///date:{ type: Date, default: Date.now }
	username: String,
	password: String
});
// the schema is useless so far
// we need to create a model using it
var user = mongoose.model('user', user);


mongoose.model('arrow', arrow);
//mongoose.model('arrow1', arrow1);
mongoose.model('contactRequests', contactRequests);

mongoose.connect('mongodb://localhost/arrow');
