﻿var mongoose = require('mongoose');
var arrow = mongoose.model('arrow');
//var arrow1 = mongoose.model('arrow1');
var contactRequests = mongoose.model('contactRequests');
var users = mongoose.model('user');
var express = require('express');
var fs = require('fs');
var multer = require('multer');
var passport = require('passport');
var bCrypt = require('bcrypt-nodejs');
//var upload = multer({ dest: 'public/uploads/',
//	rename: function (fieldname, filename) {
//		return filename.replace(/\W+/g, '-').toLowerCase() + Date.now()
//	} }).array('file1');
 i = 1;
var storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, 'public/uploads/')
	},
	filename: function (req, file, cb) {
		//cb(null, file.originalname + '-' + Date.now())
		cb(null, 'image' + i)
		//cb(null,'image'+i)
		i++;
		console.log(i);
	}
})
//console.log(i);

var upload = multer({ storage: storage }).array('file1');
var router = express.Router();

/* GET home page. */
router.get('/index', function (req, res) {
    res.render('index', { title: 'Arrow' });
});
/* GET form page. */
router.get('/', function (req, res) {
	res.render('login', { title: 'Arrow' });
});
router.get('/form', function (req, res) {
	res.render('form', { title: 'Arrow' });
});

/////////saving contacts
router.post('/save-contacts', function (req, res) {
	var input=req.body;
	var resp={"status":"0","msg":"unable to save"};
	console.log(input);
	var newContact= new contactRequests({
		queried:input.query,
		name:input.name,
		email:input.email,
		phone:input.phone

	});
	newContact.save(function (err, savedDoc) {
		if(err)
		{
			console.log('error while saving contact details ',err);
		}else{
			if(savedDoc){
				resp['status']='1';
				resp['msg']='success';

			}
		}
		res.json(resp);
	});
});
/////
router.post('/add_dairees', function (req, res) {
	console.log(req.body.link);
	new arrow({ title : req.body.title, date :req.body.date, img_link:req.body.link, read_more:req.body.read_more, mode:req.body.mode })
  .save(function (err, arrow) {
		console.log(arrow)
		res.redirect('/index');
	});
});
///////////////////
router.post('/get_diaries', function (req, res) {
	arrow.find(function (err, arrow) {
		console.log(arrow)
		if (arrow) {
			res.json({ "status": "1", "response": arrow });
			res.end();
			return;
		}
		else {
			res.json({ "status": "0", "response": "0"});
			res.end();
			return;
		}
	});
});
///////////////////
router.post('/get_diaries_mode', function (req, res) {
	//lmt = req.body.limit;
	//strt = req.body.start;
	console.log(req);
	mode = req.body.mode;
	console.log(mode);
	arrow.find({ mode: mode}).sort({_id: 1}).exec( function (err, arrow) {
		console.log(arrow)
		
		if (arrow) {
			res.json({ "status": "1", "response": arrow });
			res.end();
			return;
		}
		else {
			res.json({ "status": "0", "response": "0" });
			res.end();
			return;
		}
	});
});
///////////////////
router.post('/get_diaries_req', function (req, res) {
	lmt = req.body.limit;
	strt = req.body.start;
	console.log(req);
	mode = req.body.mode;
	query = {};
	if (mode != 'both') {
		query['mode'] = mode;
	}
	console.log(mode);
	arrow.find(query).sort({ _id: 1}).skip(strt).limit(lmt).exec(function (err, arrow) {
		console.log(arrow)
		
		if (arrow) {
			res.json({ "status": "1", "response": arrow });
			res.end();
			return;
		}
		else {
			res.json({ "status": "0", "response": "0" });
			res.end();
			return;
		}
	});
});
///////////////////
router.post('/get_diaries_date', function (req, res) {
	arrow.find(function (err, arrow) {
		//console.log(arrow)
		if (arrow) {
			arrow.forEach(function (element) {
				element.date = new Date(element.date);
				arrow1(element).update();
				console.log(arrow1);
			})
			res.json({ "status": "1", "response": arrow });
			res.end();
			return;
		}
		else {
			res.json({ "status": "0", "response": "0" });
			res.end();
			return;
		}
	}).sort({_id:1});
});
////////////////////
router.post('/file_upload', function (req, res,done) {
	upload(req, res, function (err) {
		if (err) {
			// An error occurred when uploading 
			//return
			console.log('Error in upload: ' + err);
			req.flash('warning', 'Error during Upload, try again');
			return done(err);
		}
		else {
			response = {
				message: 'File uploaded successfully',
				filename: req.files.originalname
			};
			console.log('Image Upload succesful');
			req.flash('warning', 'Image Upload succesful');
			
		}
		console.log(response);
		res.redirect('/index');
		res.end(JSON.stringify(response));
 
    // Everything went fine 
	})
})

var allfile = [];
router.post('/get_images', function (req, res) {
	res.send("hello world");
	var fullUrl = req.path;
	console.log(fullUrl);
	console.log("Going to read directory /tmp");
	var folder_path = fullUrl + "/public/uploads/";
	fs.readdir(folder_path, function (err, files) {
		if (err) {
			return console.error(err);
		}
		files.forEach(function (file) {
			//res.render('file', { "<input type='image' src='"+file+"' " });
			console.log(file);
			allfile.push(file);
			res.send("xxxx"+allfile);
		});
		//res.redirect('/file');
	});
});
//////
router.get('/add_user' , function (req, res) {
	//company.findOne({ 'companyKey': '1234567890' }, '_id', function (err, company) {
		//if (company == null) {
		//	res.json({ "status": 1, "err": "company not found." });
		//	return;
		//} 
		//else {
			var username = 'arrow';
			var createHash = function (password) {
				return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
			}
			var pass = createHash('arrow');
			var adduser = new users({
	         	username: username,
				password:pass
				});
			adduser.save(function (err, savedadmin) {
		console.log("user added");
		console.log(adduser)
		res.redirect('/login');
			});
		//}
	//});
	//res.end();
});

/* GET login page. */
router.get('/login', function (req, res) {
	res.render('login', { title: 'Arrow' });
});

///for passport
router.post('/login', function (req, res, next) {
	console.log(req.body.username);
	console.log(req.body.password);
	passport.authenticate('login', function (err, user, info) {
	//	console.log(passport);
		if (err) { return next(err); }
		if (!user) { return res.redirect('/login'); }
		req.logIn(user, function (err) {
			if (err) { return next(err); }
			if (req.session.lasturl != null)
				return res.redirect(req.session.lasturl);
			else
				return res.redirect('/index');
		});
	})(req, res, next);
});
//router.post('/login', function (req, res) {
//	var query = { "username": req.body.username , "password": req.body.password };
//	console.log(query);
//	users.findOne(query, function (err, user) {
//		console.log(user)
//		if (user) {
//			res.redirect('/index');
//		}
//		else {
//			res.redirect('/login');
//		}
	
//	});
//});
////logout
router.get("/logout", function (req, res) {
	req.logout();
	res.redirect("/login");
});

module.exports = router;