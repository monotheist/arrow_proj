toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1100",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}
$(document).ready(function () 
{
	sideBar=$("aside.left-panel");
	navbarToggle=$(".left-panel .navbar-toggle");
	navbarToggle.click(function() {
		sideBar.toggleClass("collapsed");
	});
    sideBar.find(".navigation > ul > li > a").click(function () {
        thisLi = $(this).closest("li");
		click_current=thisLi.hasClass("active");
		sideBar.find(".navigation > ul > li > ul").slideUp(300);
		sideBar.find(".navigation > ul > li").removeClass("active");
		thisLi.addClass("active");
		if(click_current) 
		{
			if(thisLi.find(" > ul").is(":visible")) {
				thisLi.find(" > ul").slideUp(300);
			} else {
				thisLi.find(" > ul").slideDown(300);
			}
		} 
		else 
		{
            thisLi.find(" > ul").slideDown(300);
        }
        if ($(this).attr("href") == "#") {
            return false;
        }
        
    });
    displayToasts();
});

function IsJsonString(str) {
	try {
		JSON.parse(str);
	} catch(e) {
		return false;
	}
	return true;
}
function hexContrast(c) {
	try {
		var c = c.substring(1);      // strip #
		var rgb = parseInt(c, 16);   // convert rrggbb to decimal
		var r = (rgb >> 16) & 0xff;  // extract red
		var g = (rgb >>  8) & 0xff;  // extract green
		var b = (rgb >>  0) & 0xff;  // extract blue

		var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709
		return luma < 125;
	} catch (e) {
		return true;
	}
}
toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1100",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}
function displayToasts() {
    if (Array.isArray(toastarray)) {
        toastarray.forEach(function (value) {
            toastr['info'](value);
        });
    }
}
function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
function hexContrast(c) {
    try {
        var c = c.substring(1);      // strip #
        var rgb = parseInt(c, 16);   // convert rrggbb to decimal
        var r = (rgb >> 16) & 0xff;  // extract red
        var g = (rgb >> 8) & 0xff;  // extract green
        var b = (rgb >> 0) & 0xff;  // extract blue
        
        var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709
        return luma < 125;
    } catch (e) {
        return true;
    }
}